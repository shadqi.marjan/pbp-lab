from django.urls import path
from .views import *

urlpatterns = [
    path('', index),
    path('add-note', add_note),
    path('note-list', note_list),
]