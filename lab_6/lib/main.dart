import 'package:flutter/material.dart';

void main() => runApp(const MyApp());

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    const title = 'Workout Videos';

    return MaterialApp(
      title: 'Workout Videos',
      theme: ThemeData(
        primarySwatch: Colors.red,
      ),
      home: Scaffold(
        appBar: AppBar(
          title: const Text(title),
        ),
        body: ListView(
          padding: EdgeInsets.all(16.0),
          children: const <Widget>[
            ListTile(
              leading: Icon(Icons.fitness_center_outlined),
              title: Text('Video 1'),
            ),
            ListTile(
              leading: Icon(Icons.fitness_center_outlined),
              title: Text('Video 2'),
            ),
            ListTile(
              leading: Icon(Icons.fitness_center_outlined),
              title: Text('Video 3'),
            ),
          ],
        ),
      ),
    );
  }
}