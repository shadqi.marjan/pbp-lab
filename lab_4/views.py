from django.shortcuts import render, redirect
from .models import Note
from .forms import NoteForm

# Create your views here.
def index(request):
    notes = Note.objects.all().values()
    response = {"notes": notes, "inbox": Note.objects.all().count()}
    return render(request, 'lab4_index.html', response)

def add_note(request):
    context = {}
    form = NoteForm(request.POST or None, request.FILES or None)

    if form.is_valid():
        form.save()
        response = redirect("/lab-4")
        return response

    context['form'] = form
    return render(request, "lab4_form.html", context)

def note_list(request):
    notes = Note.objects.all().values()
    response = {"notes": notes, "inbox": Note.objects.all().count()}
    return render(request, 'lab4_note_list.html', response)